<?php

/**
 * @file
 *   Functions used when indexing content to Apache Solr.
 */

/**
 * Given a file, return a document representing that file.
 */
function apachesolr_media_file_to_document($file, $namespace) {
  $document = FALSE;
  $build_document = TRUE;

  if ($build_document) {
    $document = new ApacheSolrDocument();
    $document->id = apachesolr_document_id($file->fid, $entity = 'file');
    $document->site = url(NULL, array('absolute' => TRUE));
    $document->hash = apachesolr_site_hash();
    $document->entity_type = 'file';
    $document->entity_id = $file->fid;
    $document->bundle = file_type_get_name($file);
    $document->bundle_name = file_get_type($file);
    $document->label = apachesolr_clean_text($file->filename);
    $path = 'media/' . $file->fid;
    $document->url = url($path, array('absolute' => TRUE));
    $document->path = $path;
    // Build the node body.
    //$build = node_view($node, 'search_index');
    // Why do we need this?
    //unset($build['#theme']);
    //$text = drupal_render($build);
    switch ($document->bundle_name){
      case 'text';
        $text = file_get_contents(drupal_realpath($file->uri));
        $document->content = apachesolr_clean_text($text);
        $document->teaser = truncate_utf8($document->content, 300, TRUE);
      default:
      #TODO add a hook
    }
    // Path aliases can have important information about the content.
    // Add them to the index as well.
    if (function_exists('drupal_get_path_alias')) {
      // Add any path alias to the index, looking first for language specific
      // aliases but using language neutral aliases otherwise.
      $language = NULL; //empty($node->language) ? NULL : $node->language;
      $output = drupal_get_path_alias($path, $language);
      if ($output && $output != $path) {
        $document->path_alias = $output;
      }
    }

    $document->ss_name = $file->filename;
    // We want the name to be searchable for keywords.
    $document->tos_name = $file->filename;

    // Everything else uses dynamic fields
    $document->is_uid = $file->fid;
    /*
    $document->bs_status = $node->status;
    $document->bs_sticky = $node->sticky;
    $document->bs_promote = $node->promote;
    $document->is_tnid = $node->tnid;
    $document->bs_translate = $node->translate;
    
    if (empty($node->language)) {
      // 'und' is the language-neutral code in Drupal 7.
      $document->ss_language = LANGUAGE_NONE;
    }
    else {
      $document->ss_language = $node->language;
    }
    $document->ds_created = apachesolr_date_iso($node->created);
    $document->ds_changed = apachesolr_date_iso($node->changed);
    if (isset($node->last_comment_timestamp) && !empty($node->comment_count)) {
      $document->ds_last_comment_timestamp = apachesolr_date_iso($node->last_comment_timestamp);
      $document->ds_last_comment_or_change = apachesolr_date_iso(max($node->last_comment_timestamp, $node->changed));
    }
    else {
      $document->ds_last_comment_or_change = apachesolr_date_iso($node->changed);
    }
    $document->is_comment_count = isset($node->comment_count) ? $node->comment_count : 0;
    */
    // Handle fields including taxonomy. TODO
  $indexed_fields = apachesolr_entity_fields('file');
    foreach ($indexed_fields as $index_key => $field_info) {
      $field_name = $field_info['field']['field_name'];
      // See if the file has fields that can be indexed
      if (isset($node->{$field_name})) {
        // Got a field.
        $function = $field_info['indexing_callback'];
        if ($function && is_callable($function)) {
          // NOTE: This function should always return an array.  One
          // node field may be indexed to multiple Solr fields.
          $fields = $function($node, $field_name, $index_key, $field_info);
          foreach ($fields as $field) {
            // It's fine to use this method also for single value fields.
            $document->setMultiValue($field['key'], $field['value']);
          }
        }
      }
    }
    /*
    apachesolr_add_tags_to_document($document, $text);
    */
    // Fetch extra data normally not visible, including comments.
    // We do this manually (with module_implements instead of node_invoke_nodeapi)
    // because we want a keyed array to come back. Only in this way can we decide
    // whether to index comments or not.
    $extra = array();
    if (isset($extra['comment'])) {
      $comments = $extra['comment'];
      unset($extra['comment']);
      $document->ts_comments = apachesolr_clean_text($comments);
      // @todo: do we want to reproduce apachesolr_add_tags_to_document() for comments?
    }
    // Use an omit-norms text field since this is generally going to be short; not
    // really a full-text field.
    $document->tos_content_extra = apachesolr_clean_text(implode(' ', $extra));

    // Let modules add to the document.

    foreach (module_implements('apachesolr_media_update_index') as $module) {
      $function = $module . '_apachesolr_media_update_index';
      $function($document, $file, $namespace);
    }
  }
  return $document;
}

/**
 * Returns array containing information about file fields that should be indexed
 */
function apachesolr_media_entity_fields($entity_type = 'file') {
  $fields = &drupal_static(__FUNCTION__, array());

  if (!isset($fields[$entity_type])) {
    $fields[$entity_type] = array();

    $mappings = module_invoke_all('apachesolr_field_mappings');

    foreach (array_keys($mappings) as $key) {
      // Set all values with defaults.
      $mappings[$key] += array(
        'dependency plugins' => array('bundle', 'role'),
        'map callback' => FALSE,
        'hierarchy callback' => FALSE,
        'indexing_callback' => '',
        'index_type' => 'string',
        'name_callback' => '',
        'facets' => FALSE,
        'facet missing allowed' => FALSE,
        // Field API allows any field to be multi-valued.
        'multiple' => TRUE,
      );
    }

    // Allow other modules to add or alter mappings.
    drupal_alter('apachesolr_field_mappings', $mappings);
    $modules = system_get_info('module');
    $instances = field_info_instances($entity_type);
    foreach (field_info_fields() as $field_name => $field) {
      $row = array();
      if (isset($field['bundles'][$entity_type]) && (isset($mappings['per-field'][$field_name]) || isset($mappings[$field['type']]))) {
        // Find the mapping.
        if (isset($mappings['per-field'][$field_name])) {
          $row = $mappings['per-field'][$field_name];
        }
        else {
          $row = $mappings[$field['type']];
        }
        // The field info array.
        $row['field'] = $field;
        // Since we use the index key as the block delta in apachesolr_search, we need a name
        // to build whatever the index key is that is used for faceting.
        // @todo: for fields like taxonomy we are indexing multiple Solr fields
        // per entity field, but are keying on a single Solr field name here.
        $function = $row['name_callback'];
        if ($function && is_callable($function)) {
          $row['name'] = $function($field);
        }
        else {
          $row['name'] = $field['id'] . '_' . $field['field_name'];
        }
        $row['module_name'] = $modules[$field['module']]['name'];
        // Set display name
        $display_name = array();
        foreach ($field['bundles'][$entity_type] as $bundle) {
          if (empty($instances[$bundle][$field_name]['display']['search_index']) || $instances[$bundle][$field_name]['display']['search_index'] != 'hidden') {
            $row['display_name'] = $instances[$bundle][$field_name]['label'];
            $row['bundles'][] = $bundle;
          }
        }
        // Only add to the $fields array if some instances are displayed for the search index.
        if (!empty($row['bundles'])) {
          // Use the Solr index key as the array key.
          $fields[$entity_type][apachesolr_index_key($row)] = $row;
        }
      }
    }
  }
  return $fields[$entity_type];
}

