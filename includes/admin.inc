<?php
function apachesolr_media_preprocess(&$variables, $hook) {
  if (($hook == 'page') && (arg(0) == 'admin') && (arg(1) =='config') && (arg(2) == 'media') && (arg(3) == 'file-types') && is_null(arg(4))) {
    $variables['page']['content']['system_main']['file_type_table']['#header'][1]['colspan'] = 4;
    $file_types = file_info_file_types();
    //$rows = ($variables['page']['content']['system_main']['file_type_table']['#rows']);
    $i = 0;
    foreach ($file_types as $index => $row) {
      $variables['page']['content']['system_main']['file_type_table']['#rows'][$i][4] = l('manage search', 'admin/config/media/file-types/manage/' . $index . '/search');
      $i++;
    }
  }

}

function apachesolr_media_file_search_form($form, &$form_state, $file_type) {
  switch ($file_type) {
    case 'text':
      $form['apachesolr_media_index']['apachesolr_media_' . $file_type] = array (
        '#type' => 'checkbox', 
        '#title' => t('Enable to index text file in Apachesolr'), 
        '#default_value' => variable_get('apachesolr_media_' . $file_type, False),
        '#description' => t('There is no special setting to do to index plain text file.')
      );
      break;
    case 'pdf':
      break;
    case 'doc':
      break;
    default:
      $form['apachesolr_media_' . $file_type] = array (
        '#type' => 'textfield', 
        '#title' => t($file_type . ' file parser setting'), 
        '#default_value' => variable_get('apachesolr_media_' . $file_type,''), 
        '#size' => 200, 
        '#maxlength' => 200, 
        '#required' => TRUE,
        '#description' => t('Are you sure the command you put in can work? If not the file will not be indexed.')
      );
  }
  return system_settings_form($form);
}

function apachesolr_media_list_types_page() {
  return '';
}
